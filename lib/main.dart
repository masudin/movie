import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:providermovie/const.dart';
import 'package:providermovie/pages/home.dart';
import 'package:provider/provider.dart';
import 'package:providermovie/provider/movie_get_discover.dart';
import 'package:providermovie/repository/movie_repository.dart';
import 'package:providermovie/repository/movie_repository_impl.dart';


void main() {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  final dioOption = BaseOptions(
    baseUrl: Const.base_url,
    queryParameters: {'api_key' : Const.api_key}
  );
  final Dio dio = Dio(dioOption);
  final MovieRepository movieRepository  = MovieRepositoryImpl(dio);

  runApp(MyApp(movieRepository: movieRepository,));
  FlutterNativeSplash.remove();
}


class MyApp extends StatelessWidget {
  const MyApp({super.key, required this.movieRepository});

  final MovieRepository movieRepository ;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => MovieGetDiscover(movieRepository)
          )
      ],
      child: MaterialApp(
        title: "Flutter Movie",
        theme: ThemeData(primarySwatch: Colors.green),
        home: const HomePage(),
      ),
    );
  }
}