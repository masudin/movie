import 'package:flutter/material.dart';
import 'package:providermovie/models/movie_models.dart';
import 'package:providermovie/repository/movie_repository.dart';

class MovieGetDiscover with ChangeNotifier{
  
  final MovieRepository _movieRepository;
  MovieGetDiscover(this._movieRepository);

  bool _isLoading = false;
  bool get IsLoading => _isLoading;

  final List<MovieModel> _movies = [];
  List<MovieModel> get movies => _movies;

  void getDiscover(BuildContext context) async{
    _isLoading = true;
    notifyListeners();
    
    final result = await _movieRepository.getDiscover();
    result.fold(
      (errorMessage) {
         ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(errorMessage)));
        _isLoading = false;
        notifyListeners();
        return;
      },
      (response) {
        _movies.clear();
        _movies.addAll(response.results);
        _isLoading = false;
        notifyListeners();
        return null;
      });
  }
}