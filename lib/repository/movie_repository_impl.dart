import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:providermovie/models/movie_models.dart';
import 'package:providermovie/repository/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository{
  final Dio _dio;

  MovieRepositoryImpl(this._dio);

  @override
  Future<Either<String, MovieResponseModel>> getDiscover({int page = 1}) async {
    try{
      final result = await _dio.get('/discover/movie', queryParameters: {'page' : page});
      if(result.statusCode == 200 && result.data != null){
        final model = MovieResponseModel.fromMap(result.data);
          return Right(model);
      }
        return const Left("Error get discover movies");
    }on DioError catch(e){

      if(e.response != null){
        return Left(e.response.toString());
      }
        return const Left("Another error on get discover movie");
    }
  }
  
}