//menampung fungsi2 yang akan digunakan
import 'package:dartz/dartz.dart';
import 'package:providermovie/models/movie_models.dart';

abstract class MovieRepository{
    Future<Either<String, MovieResponseModel>> getDiscover({int page = 1});
}