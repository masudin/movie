import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providermovie/provider/movie_get_discover.dart';


class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((__) {
          context.read<MovieGetDiscover>().getDiscover(context);
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Movie'),
        ),
        body: Consumer<MovieGetDiscover>(
          builder: (_, provider, __) {
            if(provider.IsLoading){
              return const Center(child: CircularProgressIndicator(),);
            }

            if(provider.movies.isNotEmpty){
              return ListView.builder(itemBuilder: (context, index){
                final movie = provider.movies[index];
                return ListTile(
                  title: Text(movie.title),
                  subtitle: Text(movie.overview),
                  );
              });
            }
            return const Center(child: Text("Not found discover movie"),);
        },
      ),
    );
  }
}