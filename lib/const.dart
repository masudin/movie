class Const{
  static const String api_key = "d8e957a69af1be921c9c4466f5661e87";
  static const String base_url = "https://api.themoviedb.org/3";
  static const String img_url = "https://api.themoviedb.org/t/p/w500";
  static const String img_url_detail = "https://api.themoviedb.org/t/p/original";
}